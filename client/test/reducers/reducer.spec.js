import expect from 'expect';
import deepFreeze from 'deep-freeze';
import * as actions from '../../actions/actionCreators';
import { myReducer } from '../../reducers/testreducer';
import { clickATag } from '../../reducers/clickatag';
import { myTime } from '../utils/time';

describe('--- ' + myTime() + ' --- Start running my tests', () => {
    describe('Testing the testreducer', () => {
        it ('Will test my add and subtract functions at once', () => {
            var init = {
                myPoints: 12,
                yourPoints: 27
            };

            deepFreeze(init);

            var next = myReducer(init);

            var myNext = {
                myPoints: 39,
                yourPoints: -15
            };

            expect(myNext).toEqual(next);
        });
    });

    describe('Testing the click-a-tag action', () => {
        it ('should return an object when calling the action', () => {
            const action = actions.clickATag('unopsicoo');

            const expectedAction = {
                type: 'CLICK_A_TAG',
                tag: 'unopsicoo'
            };

            expect(action).toEqual(expectedAction);
        });
    });

    describe('Testing the click-a-tag reducer', () => {
        it ('should return the passed tag when initially called', () => {
            const init = undefined;

            const action = actions.clickATag('unopsicoo');
            const next   = clickATag(init, action);
            const expectedNext = 'unopsicoo';

            expect(next).toEqual(expectedNext);
        });

        it ('should add the new tag to the current string', () => {
            const init = 'oktabori klorisi factile';
            deepFreeze(init);

            const action = actions.clickATag('unopsicoo');
            const next   = clickATag(init, action);
            const expectedNext = 'oktabori klorisi factile unopsicoo';

            expect(next).toEqual(expectedNext);
        });
    });
});


