import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute } from 'react-router';
import { Provider } from 'react-redux';

// import statement for css
import css from './assets/app.less';

// import components
import Main from './components/Main.js';
import StartScreen from './components/StartScreen';
import Useless from './components/Useless';
import Clickable from './components/Clickable';
import TagList from './components/TagList';

// import store
import store, { history } from './store';

const router = (
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={Main}>
                <IndexRoute component={StartScreen}></IndexRoute>
                <Route path="/useless" component={Useless}></Route>
                <Route path="/clickable(/:view)" component={Clickable}></Route>
                <Route path="/tags" component={TagList}></Route>
            </Route>
        </Router>
    </Provider>
)

render(router, document.getElementById('root'));

