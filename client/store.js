// Store

import { createStore, applyMiddleware } from 'redux';
import { syncHistoryWithStore} from 'react-router-redux';
import { browserHistory } from 'react-router';
import { rootReducer } from './reducers/reducer';

// import some middleware!
import createLogger from 'redux-logger';

const loggerMiddleware = createLogger();

// create store

console.log('create the store');
const store = createStore(rootReducer, applyMiddleware(loggerMiddleware));
console.log('just after creating the store');

// enhance history (named export)
export const history = syncHistoryWithStore(browserHistory, store);

// default export
export default store;
