import React from 'react';
import { Link } from 'react-router';

const Clickable = React.createClass({

  render() {
    const view = (this.props.view === undefined) ? 0 : parseInt(this.props.view);
    let colorClass = '';
    let icon = <span className="glyphicon glyphicon-heart"></span>;
    if (view) {
      colorClass = 'view';
      icon = <span className="glyphicon glyphicon-star"></span>;
    }
    let count = <span>{this.props.count}</span>
    let clickUrl = '/clickable/' + (1 - view);
    return (
      <div className="interaction">

        <Link to={clickUrl}><button>Click me! {count} {icon}</button></Link>

        <p className={colorClass}>
            Vivamus id sem in nulla ornare commodo. Nullam ultricies feugiat nibh, ut iaculis ante consectetur at. Proin faucibus, neque vitae aliquet pellentesque, neque enim iaculis dui, sit amet convallis ante ligula et ligula. Vestibulum nec tempor mauris, ac eleifend dolor. Quisque non porta enim. Donec faucibus nisl eu orci volutpat commodo non nec lectus. Pellentesque eu nibh posuere, luctus purus ultrices, eleifend tortor. Morbi mi velit, posuere sit amet cursus id, maximus vel quam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin viverra scelerisque cursus. Vestibulum at risus aliquet, pretium tellus eu, varius felis.
            Mauris consequat nisi eu viverra congue. Nullam feugiat sapien vehicula tortor vestibulum, sit amet tristique velit rhoncus. Praesent rhoncus tempor nibh mattis semper. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas id nisi at quam maximus aliquet in ut nunc. Morbi lobortis, purus sit amet eleifend scelerisque, libero enim malesuada arcu, at ullamcorper ipsum justo et nibh. Donec dolor lacus, gravida quis mauris non, ornare imperdiet velit. Integer eget eros rhoncus, mattis erat nec, accumsan nulla. Sed nec condimentum eros.
        </p>

      </div>
    )
  }
});

export default Clickable;
