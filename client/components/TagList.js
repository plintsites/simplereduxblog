import React from 'react';
import Tag from './Tag';

const TagList = React.createClass({

  renderTag(tag) {
    return <Tag key={tag} tag={tag} clickATag={this.props.clickATag}/>
  },

  render() {
    const taglist = ['Vivamus', 'id', 'sem', 'in', 'nulla', 'ornare', 'commodo', 'Nullam', 'ultricies', 'feugiat', 'nibh', 'ut', 'iaculis', 'ante', 'consectetur'];
    return (
      <div className="taglist">

        {taglist.map(this.renderTag)}

        <p className="myText">{this.props.text}</p>

      </div>
    )
  }
});

export default TagList;
