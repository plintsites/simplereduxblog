import React from 'react';
import { Link } from 'react-router';

const App = React.createClass({

  render() {
    return (
      <div>
        <h1>
          <Link to="/">Silly App</Link>
        </h1>
        {React.cloneElement(this.props.children, {
          ...this.props,
          key: undefined,
          ref: undefined,
          count: this.props.count,
          view: this.props.view
        })}
      </div>
    )
  }
});

export default App;
