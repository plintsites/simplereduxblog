import React from 'react';

const Tag = React.createClass({

    clickATagHandler() {
        this.props.clickATag(this.props.tag);
    },

    render() {
        return (
            <div className="tag" onClick={this.clickATagHandler}>{this.props.tag}</div>
        )
    }

});

export default Tag;
