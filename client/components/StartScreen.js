import React from 'react';
import { Link } from 'react-router';

const StartScreen = React.createClass({
  	render() {
    	return (
			<div className="buttons">
				<Link to="/useless"><button type="button">Read Me <span className="glyphicon glyphicon-file"></span></button></Link>
				<br/>
				<Link to="/clickable"><button type="button">Interaction <span className="glyphicon glyphicon-play"></span></button></Link>
                <Link to="/tags"><button type="button">Tags <span className="glyphicon glyphicon-bookmark"></span></button></Link>
			</div>
    	)
  	}
});

export default StartScreen;
