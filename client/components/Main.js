import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';
import App from './App';

const mapStateToProps = function(state, ownProps) {

    return {
        count: state.UI.count,
        view: ownProps.params.view,
        text: state.tags
    };
};

const mapDispatchToProps = function(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
};

const Main = connect(mapStateToProps, mapDispatchToProps)(App);

export default Main;
