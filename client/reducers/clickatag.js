export const clickATag = function(state = '', action) {

    switch (action.type) {
        case 'CLICK_A_TAG':
            if (state.length === 0) {
                return action.tag;
            } else {
                return state + ' ' + action.tag;
            }
        default:
            return state;
    }
}
