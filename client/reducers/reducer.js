// The main reducer

import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { appReducer } from './count';
import { clickATag } from './clickatag';

export const rootReducer = combineReducers({
    UI: appReducer,
    tags: clickATag,
    routing: routerReducer
});
