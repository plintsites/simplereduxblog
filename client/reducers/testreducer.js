function add(a, b) {
    return a + b;
}

function subtract(a, b) {
    return a - b;
}

export function myReducer(state) {
    var {myPoints, yourPoints} = state;

    return {
        ...state,
        myPoints: add(myPoints, yourPoints),
        yourPoints: subtract(myPoints, yourPoints)
    }
}
