// define initial state, we have a counter (integer, starting at 0) and a view (boolean represented by 0/1)
const initState = {
    count: 0
};

export const appReducer = function(state = initState, action) {

    const {count} = state;
    switch (action.type) {
        case '@@router/LOCATION_CHANGE':
            if (action.payload.pathname.indexOf('clickable/') > -1) {
                return {
                    ...state,
                    count: count + 1
                };
            }
        default:
            return state;
    }
};
