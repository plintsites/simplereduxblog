// my actions
const CLICK_A_TAG = 'CLICK_A_TAG';

export function clickATag(tag) {
    return {
        type: CLICK_A_TAG,
        tag
    };
}
